
import { FaWhatsapp, FaInstagram, FaDiscord } from 'react-icons/fa';



export default function Footer(){


    return(
        <div>
            <footer className="footer">

            <div className="container">
                <div className ="row">

                    <div className = "footer-col">
                        <h1>PESONA MUDA</h1>
                    </div>

                    <div className = "footer-col">
                        <ul>
                            <h6>IMPLEMENT FOOTER HERE</h6>
                            <li> <a href="#">about</a></li>
                            <li> <a href="#">services</a></li>
                            <li> <a href="#">contact</a></li>
                            <li> <a href="#">shop</a></li>
                        </ul>
                    </div>

                    <div className = "footer-col">
                        <ul>
                            <h6>IMPLEMENT FOOTER HERE</h6>
                            <li> <a href="#">about</a></li>
                            <li> <a href="#">services</a></li>
                            <li> <a href="#">contact</a></li>
                            <li> <a href="#">shop</a></li>
                        </ul>
                    </div>

                    <div className = "footer-col">
                        <h6>FOLLOW US</h6>
                        <div className = "social-links">
                        <a href= "#"><div><FaWhatsapp/></div></a>
                        <a href= "#"><div><FaInstagram/></div></a>
                        <a href= "#"><div><FaDiscord/></div></a>
                        </div>   
                    </div>
                        </div>
                        <br></br>
                        <br></br>
                <p1>copyright &copy; pesona muda 2022</p1>
                <p2>designed by <br></br>thoriq aulia<br></br> rafi taufiqul h</p2>
            </div>

            
            </footer>
            
            </div>
    )
}