import { FaPlay } from 'react-icons/fa';

export default function Card(props){

     return (
        <div className="card">
            <div className='card-img-container'>
                {
                props.img ? <img alt='people' src={props.img} />:
                props.gender === 'male' ?
                <img alt='people' src='/male-user.jpg' />:
                <img alt='people' src='/female-user.jpg' />
                }
                <div className='card-img-overlay'>
                    <FaPlay/>
                </div>
            </div>
            <p>{props.nama}</p>
        </div>
    )
}