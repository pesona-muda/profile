export default function Navbar(){

    return(
    
        <div className="nav">
            <input type="checkbox" id="nav-check"></input>
            <div className="nav-header">
                <div className="nav-title">
                Pesona Muda
                </div>
            </div>
            <div className="nav-btn">
                <label htmlFor="nav-check">
                    <span></span>
                    <span></span>
                    <span></span>
                </label>
            </div>
  
            <div className="nav-links">
                <a href="#home">Home</a>
                <a href="#news">News</a>
                <a href="#contact">Contact</a>
                <a href="#fam">Fam</a>
                <a href="#story">Story</a>
            </div>
        </div>
                
        
    )
}