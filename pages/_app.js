import '../styles/globals.css'
import '../styles/navbar.css'
import '../styles/footer.css'
import '../styles/card.css'
import '../styles/family.css'
import Head from '../components/head'

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head/>
      <Component {...pageProps} />
    </>
  )
  

}

export default MyApp
