import Navbar from "../components/navbar"
import Footer from "../components/footer"
import Card from "../components/card"
import { FaSearch } from 'react-icons/fa';


export default function Home(props) {
  const dummyData = [
    {nama:'Emir', gender:'male', img:'https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png?rev=2540745'},
    {nama:'Farah', gender:'female'},
    {nama:'Rafi', gender:'male'},
    {nama:'Aldy', gender:'male'},
    {nama:'Anggun', gender:'female'},
    {nama:'Thoriq', gender:'male'},
    {nama:'Putra', gender:'male', img:'https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png?rev=2540745'},
    {nama:'Faisal', gender:'male'},
    {nama:'Ziyad', gender:'male'},
    {nama:'Adam', gender:'male', img:'https://ps.w.org/user-avatar-reloaded/assets/icon-256x256.png?rev=2540745'},
  ]

  return (
    <div>
      <Navbar/>
      
      <div className="family-header">
        {/* <img src="/header-1.png"></img> */}

        <div className='family-header-text'>
          <div>INTRODUCING</div>
          <div className="family-header-title">PESONA MUDA FAMILY</div>
          <div className="family-search-input">
            <input type='text' placeholder="Cari nama"></input>
            <FaSearch/>
          </div>
        </div>
      </div>

      <div className="search-result-container">
      {
        dummyData.map((item,index) =>{
          return(
            <Card  key={index} nama={item.nama} gender={item.gender} img={item.img} />
          )
        })
      }
      </div>
  
      <Footer/>
    </div>
  )
}

